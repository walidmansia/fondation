resource "aws_iam_group" "devops_grp" {
  name = "devops_grp"

}
resource "aws_iam_group_membership" "devops_team" {
  name = "devops_team"
  users =  var.devops_users
  group = aws_iam_group.devops_grp.name
}

resource "aws_iam_group_policy" "devops_policy" {
  name  = "devops_policy"
  group = aws_iam_group.devops_grp.id
  policy = var.deployer_role_policy_json
}
