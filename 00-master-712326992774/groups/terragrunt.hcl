include {
  path = find_in_parent_folders()
}

dependency "users" {
  config_path = "../users"
}
dependency "roles"{
  config_path = "../roles"
}
inputs = {
  devops_users = dependency.users.outputs.devops_users
  deployer_role_policy_json = dependency.roles.outputs.deployer_role_policy_json
}


