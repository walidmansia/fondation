variable "aws_region" {
  description = "AWS region"
}

variable "devops_users" {
  type = set(string)
  description = "list of devops users"

}
variable "deployer_role_policy_json" {
  description = "the data of the role"

}
