provider "aws" {
  version = "2.70.0"
  region     = var.aws_region
  assume_role {
    role_arn = var.provider_env_roles
  }
}

module "roles" {
  source = "../../modules/roles"
  aws_region = var.aws_region
  provider_env_roles  = var.provider_env_roles
}
terraform {
  backend "s3" {
  }
}
