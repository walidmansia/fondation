output "lambda_role" {
  value = module.roles.lambda_role
}
output "apigw_role" {
  value       = module.roles.apigw_role
  description = "The Amazon Resource Name (ARN) specifying the role."
}
output "deployer_role_policy_json" {
  description = "The deployer IAM role"
  value = module.roles.deployer_role_policy_json
}
