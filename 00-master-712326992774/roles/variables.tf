variable "aws_region" {
  description = "AWS region"
}
variable "provider_env_roles" {
  description = "the role that will be assumend by the runner of terraform"
}
