terraform {
  extra_arguments "common_vars_plan" {
    commands = [
      "plan"]

    arguments = [
      "-var-file=master.tfvars"
    ]
  }

  extra_arguments "common_vars_autoapprouve" {
    commands = [
      "apply","destroy"]

    arguments = [
      "-var-file=master.tfvars",
      "-auto-approve"
    ]
  }
}
remote_state {
  backend = "s3"

  config = {
    bucket          = "yayi-712326992774-tfstates"
    key             = "712326992774/fondation/${path_relative_to_include()}.tfstate"
    region          = "eu-west-1"
  }
}
