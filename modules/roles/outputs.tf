output "lambda_role" {
  value = aws_iam_role.iam_for_lambda.arn
}
output "apigw_role" {
  value       = aws_iam_role.iam_for_api_gw.arn
  description = "The Amazon Resource Name (ARN) specifying the role."
}
output "deployer_role_policy_json" {
  description = "The deployer IAM role"
  value = data.aws_iam_policy_document.deployer-role-policy.json
}
