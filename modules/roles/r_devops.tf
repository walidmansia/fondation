
data "aws_iam_policy_document" "deployer-role-policy" {
  statement {
    sid = "acm"
    effect = "Allow"
    actions = [
      "acm:DescribeCertificate",
      "acm:GetHostedZone",
      "acm:ListTagsForCertificate",
      "acm:DeleteCertificate",
      "acm:RequestCertificate",
    ]
    resources = [
      "*"]
  }
  statement {
    sid = "apigateway"
    effect = "Allow"
    actions = [
      "apigateway:*",
    ]
    resources = [
      "*"]
  }
  statement {
    sid = "cloudfront"
    effect = "Allow"
    actions = [
      "cloudfront:GetDistribution",
      "cloudfront:TagResource",
      "cloudfront:ListTagsForResource",
      "cloudfront:DeleteDistribution",
      "cloudfront:UpdateDistribution",
      "cloudfront:CreateDistribution",
    ]
    resources = [
      "*"]
  }
  statement {
    sid = "cloudwatch"
    effect = "Allow"
    actions = [
      "cloudwatch:DescribeAlarms",
      "cloudwatch:ListTagsForResource",
      "cloudwatch:DeleteAlarms",
      "cloudwatch:PutMetricAlarm",
    ]
    resources = [
      "*"]
  }
  statement {
    sid = "cognito"
    actions = [
      "cognito-identity:*",
      "cognito-idp:*",
      "cognito-sync:*",
    ]

    resources = [
      "*",
    ]
  }
  statement {
    sid = "ec2"
    actions = [
      "ec2:DescribeAccountAttributes",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    sid = "iamGetList"
    actions = [
      "iam:List*",
      "iam:Get*",
      "iam:PassRole",
    ]

    resources = [
      "*",
    ]
  }
  statement {
    sid = "iamPost"
    actions = [
      "iam:*MFA*"
    ]
    resources = [
      "*",
    ]
    condition {
      test = "StringNotEquals"
      variable = "aws:username"
      values = [
        "admin"]

    }
  }
  statement {
    sid = "lambda"
    effect = "Allow"
    actions = [
      "lambda:*",
      "lambda:GetAlias",
      "lambda:CreateFunction",
      "lambda:UpdateFunctionCode",
    ]
    resources = [
      "*"]
  }
  statement {
    sid = "logs"
    effect = "Allow"
    actions = [
      "logs:*",
    ]
    resources = [
      "*"]
  }
  statement {
    sid = "route53"
    effect = "Allow"
    actions = [
      "route53:ListHostedZones",
      "route53:GetHostedZoneCount",
      "route53:GetHostedZone",
      "route53:ListTagsForResource",
      "route53:ListResourceRecordSets",
      "route53:GETResourceRecordSets",
      "route53:ChangeResourceRecordSets",
      "route53:GetChange",
    ]
    resources = [
      "*"]
  }
  statement {
    sid = "sns"
    effect = "Allow"
    actions = [
      "SNS:GetTopicAttributes",
      "SNS:ListTagsForResource",
      "SNS:DeleteTopic",
      "SNS:CreateTopic",

    ]
    resources = [
      "*"]
  }
  statement {
    sid = "s3"
    actions = [
      "s3:*",
    ]

    resources = [
      "arn:aws:s3:::*",
    ]
  }
  statement {
    sid = "s3fondationDeny"
    actions = [
      "s3:PutObject"
    ]
    effect = "Deny"
    resources = [
      "arn:aws:s3:::atn-tfstates/fondation/*",
    ]
  }
  statement {
    sid = "stsAssumeRole"
    actions = [
      "sts:AssumeRole"
    ]
    effect = "Allow"
    resources = [
      "*"
    ]
  }
}


